# PayMate

[![pipeline status](https://chaos.expert/telegnom/paymate/badges/master/pipeline.svg)](https://chaos.expert/telegnom/paymate/commits/master)

## API

Example implementation if the API according to the definition from [Space Markets API](https://space-market.github.io/API/)

## State of the API

The goal is to provide a simple payment api which can be used in hackerspaces and
alike clubs. There might be a rash of security issues, please improve it! 
The code needs a lot of refactoring and cleanup, too.


## Running PayMate

### Manuel

#### Installation

1. ```git clone https://chaos.expert/telegnom/paymate.git```
1. ```cd paymate```
1. ```pip install -r requirements.txt```
1. copy config to local_config ```cp config.py local_config.py```
1. adjust settings in ```local_config.py```
1. ```export FLASK_APP=paymate```
1. ```flask db init```
1. ```flask db migrate```
1. ```flask db upgrade```
1. ```./paymate.py```

#### Upgrade

1. ```git pull```
1. ```export FLASK_APP=paymate```
1. ```flask db migrate```
1. ```flask db upgrade```
1. ```./paymate.py```

#### Docker

1. ```docker-compose up -d```
1. ```docker-compose run flask db init```
1. ```docker-compose run flask db migrate```
1. ```docker-compose run flask db upgrade```
(optional, only if database outdated)
