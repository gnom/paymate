#!/usr/bin/env python3

from setuptools import setup, find_packages

setup(
    name='paymate',
    author='telegnom',
    url='https://chaos.expert/telegnom/paymate',
    description='a payment api for hackerspaces',
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        'flask',
    ],
    setup_requires=[
        'pytest-runner',
    ],
    tests_require=[
        'pytest',
    ],
)