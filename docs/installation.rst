Installation
************

The setup of a PayMate Setup is not as straight forward as it could be.

Database
========
If you don't have a database installed yet, install your prefered database, now.
MySQL or MariaDB are recommended.

database, user and privileges
-----------------------------

Create a new database to use with paymate. It is also recommended to create a
new database user for PayMate.
At least grant the ``ALTER``, ``CREATE``, ``DELETE``, ``INDEX``, ``SELECT`` and
``UPDATE`` privileges of the new database to the user as they are required to
perform the migrations and run the application later on.

Get and set up PayMate
======================

Clone the latest version
------------------------

Clone the latest version from `chaos.expert <https://chaos.expert/telegnom/paymate>`_

``git clone https://chaos.expert/telegnom/paymate.git``

Setup a virtual environment (optional)
--------------------------------------

Even it is strongly recommended to create a virtual environment for paymate
this step may be skipped.

``virtualenv venv``

Activate the virtual environment. From now on, all steps are performed *inside*
of this environment.

``. venv/bin/activate``

Install the dependencies
------------------------

PayMate provides a requirement file which can feed into pip to install the
required packages.

``pip install -r requirements.txt``

Adjust the configuration
------------------------

Copy ``config.py`` in the root folder to ``local_config.py`` and adjust the
settings in ``local_config.py`` to match your needs. Some of the properties in
the config file have no impact on the application yet, since it's in an early
stage of development.

make database migrations
------------------------

At first you have to export an environment variable called ``FLASK_APP`` containing
the name of the main file of paymate which is ``paymate.py``

``export FLASK_APP=paymate.py``

If you run PayMate for the first time you have to set up a repository where
sqlalchemy can store some informations about the migrations to come.

``flask db init``

Now you are ready to actually run the first mirgration and write it to the database.
These two steps are done by the following commands.

``flask db migrate``

and

``flask db upgrade``

Now your database is set up propperly and you can run PayMate for the first time.

``flask run``

Congratulations! You have set up your first instance of PayMate!
