FROM alpine:latest

RUN apk update && \
      apk add python3 mariadb-client bash mariadb-dev gcc jpeg-dev libjpeg \
      linux-headers musl-dev libc-dev libjpeg-turbo-dev python3-dev libmagic git && \
      git clone https://chaos.expert/telegnom/paymate.git
WORKDIR paymate
RUN pip3 install -r requirements.txt
CMD ["/bin/bash", "runpaymate.sh"]
