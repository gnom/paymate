import unittest
import json
import paymate
import datetime

class PaymateTestCase(unittest.TestCase):
    testusers = [
                 {'name': 'testuser', 'active': True, 'redirect': True,  'email': 'test@example.com', 'audit': True},
                 {'name': 'test2', 'active': True, 'redirect': True, 'email': 'user@example.com'},
                 {'name': 'klaus', 'email': 'klaus@example.com'},
                ]

    testdenom = [{'amount': 1000}, {'amount': 2000}]

    testprods = [
                 {'name': 'product1'},
                 {'name': 'product2', 'price': 200, 'caffeine': 32, 'stock': 24, 'energy': 42, 'sugar': 103, 'alcohol': 505},
                 {'name': 'updated product', 'price': 150, 'caffeine': None, 'stock': 20, 'energy': 2, 'sugar': 0, 'alcohol': 150},
                ]

    testbcodes = [
                  {'barcode': '4260031874056', 'type': 'product', 'linked': 1},
                  {'barcode': '4260031874057', 'type': 'product', 'linked': 2},
                  {'barcode': '2312341231240', 'type': 'user', 'linked': 1},
                 ]

    def setUp(self):
        paymate.app.testing = True
        self.app = paymate.app.test_client()

    def tearDown(self):
        pass

    def test_0001_serverinfo(self):
        res = self.app.get('/{}/'.format(paymate.app.config['API_URI_VERSION']))
        data = json.loads(res.data.decode('utf8'))
        self.assertEqual(data['api_version'], paymate.app.config['API_VERSION'])
        self.assertEqual(data['global_credit_limit'], paymate.app.config['GLOBAL_CREDIT_LIMIT'])

    def test_0002_serverinfopath(self):
        res = self.app.get('/{}/info/'.format(paymate.app.config['API_URI_VERSION']))
        res2 = self.app.get('/{}/'.format(paymate.app.config['API_URI_VERSION']))
        self.assertIn('200', res.status)
        self.assertEqual(res.data, res2.data)

    def test_0100_create_1st_user(self):
        res = self.app.post('/{}/users/'.format(paymate.app.config['API_URI_VERSION']), data=self.testusers[0])
        data = json.loads(res.data.decode('utf8'))
        self.assertIn("200", res.status)
        self.assertTrue(set(self.testusers[0].items()).issubset(set(data.items())))

    def test_0200_create_2nd_user(self):
        res = self.app.post('/{}/users/'.format(paymate.app.config['API_URI_VERSION']), data=self.testusers[1])
        data = json.loads(res.data.decode('utf8'))
        self.assertIn("200", res.status)
        self.assertTrue(set(self.testusers[1].items()).issubset(set(data.items())))
        res = self.app.post('/{}/users/'.format(paymate.app.config['API_URI_VERSION']), data=self.testusers[0])
        data = json.loads(res.data.decode('utf8'))
        self.assertIn("500", res.status)
        self.assertIn("username already in use", data['error'])

    def test_0210_modify_user(self):
        res = self.app.patch('/{}/users/2/'.format(paymate.app.config['API_URI_VERSION']), data=self.testusers[2])
        data = json.loads(res.data.decode('utf8'))
        self.assertTrue(set(self.testusers[2].items()).issubset(set(data.items())))
        self.assertIn("200", res.status)

    def test_0300_create_1st_denomination(self):
        res = self.app.post('/{}/denominations/'.format(paymate.app.config['API_URI_VERSION']), data=self.testdenom[0])
        data = json.loads(res.data.decode('utf8'))
        self.assertIn("200", res.status)
        self.assertTrue(set(self.testdenom[0].items()).issubset(set(data.items())))

    def test_0400_create_2nd_denomination(self):
        res = self.app.post('/{}/denominations/'.format(paymate.app.config['API_URI_VERSION']), data=self.testdenom[1])
        data = json.loads(res.data.decode('utf8'))
        self.assertIn("200", res.status)
        self.assertTrue(set(self.testdenom[1].items()).issubset(set(data.items())))

    def test_0401_recreate_1st_denomination(self):
        res = self.app.post('/{}/denominations/'.format(paymate.app.config['API_URI_VERSION']), data=self.testdenom[0])
        data = json.loads(res.data.decode('utf8'))
        self.assertIn("500", res.status)
        self.assertIn("error", data)

    def test_0500_get_denomination(self):
        res = self.app.get('/{}/denominations/1/'.format(paymate.app.config['API_URI_VERSION']))
        data = json.loads(res.data.decode('utf8'))
        self.assertIn("200", res.status)
        self.assertTrue(set(self.testdenom[0].items()).issubset(set(data.items())))

    def test_0600_delete_denominations(self):
        res = self.app.delete('/{}/denominations/2/'.format(paymate.app.config['API_URI_VERSION']))
        self.assertIn("204", res.status)

    def test_0700_create_1st_product(self):
        res = self.app.post('/{}/products/'.format(paymate.app.config['API_URI_VERSION']), data=self.testprods[0])
        data = json.loads(res.data.decode('utf8'))
        self.assertIn("200", res.status)
        self.assertTrue(set(self.testprods[0].items()).issubset(set(data.items())))

    def test_0800_create_2nd_product(self):
        res = self.app.post('/{}/products/'.format(paymate.app.config['API_URI_VERSION']), data=self.testprods[1])
        data = json.loads(res.data.decode('utf8'))
        self.assertIn("200", res.status)
        self.assertTrue(set(self.testprods[1].items()).issubset(set(data.items())))

    def test_0801_recreate_1st_product(self):
        res = self.app.post('/{}/products/'.format(paymate.app.config['API_URI_VERSION']), data=self.testprods[0])
        data = json.loads(res.data.decode('utf8'))
        self.assertIn("500", res.status)
        self.assertIn("error", data)

    def test_0810_modify_product(self):
        res = self.app.patch('/{}/products/1/'.format(paymate.app.config['API_URI_VERSION']), data=self.testprods[2])
        data = json.loads(res.data.decode('utf8'))
        self.assertIn("200", res.status)
        self.assertTrue(set(self.testprods[2].items()).issubset(set(data.items())))

    def test_0900_deposit_money(self):
        res = self.app.post('/{}/users/1/deposit/'.format(paymate.app.config['API_URI_VERSION']), data={'amount': 2000})
        self.assertIn("204", res.status)
        user = self.app.get('/{}/users/1/'.format(paymate.app.config['API_URI_VERSION']))
        data = json.loads(user.data.decode('utf8'))
        self.assertEqual(2000, data['balance'])

    def test_1000_buy_product_by_id(self):
        res = self.app.post('/{}/users/1/buy/'.format(paymate.app.config['API_URI_VERSION']), data={'product': 1})
        self.assertIn("204", res.status)
        res = self.app.get('/{}/users/1/'.format(paymate.app.config['API_URI_VERSION']))
        data = json.loads(res.data.decode('utf8'))
        self.assertEqual(data['balance'], 1850)

    def test_1100_check_audit(self):
        start = str(datetime.datetime.utcnow() - datetime.timedelta(days=2))
        res = self.app.get('/{}/audits/'.format(paymate.app.config['API_URI_VERSION']), query_string={'start': start, 'user': 1})
        data = json.loads(res.data.decode('utf8'))
        self.assertTrue(data['payments_sum'] == 150)
        self.assertTrue(data['audits'][0]['action'] == 'deposit')
        self.assertTrue(data['audits'][0]['amount'] == 2000)
        self.assertTrue(data['audits'][0]['product_id'] is None)
        self.assertTrue(data['audits'][1]['action'] == 'spend')
        self.assertTrue(data['audits'][1]['amount'] == 150)
        self.assertTrue(data['audits'][1]['product_id'] == 1)

    def test_1200_transfer_money(self):
        res = self.app.post('/{}/users/1/transfer/'.format(paymate.app.config['API_URI_VERSION']), data={'amount': 1000, 'receiver': 2})
        self.assertIn("204", res.status)

    def test_1300_create_barcode_product(self):
        res = self.app.post('/{}/barcodes/'.format(paymate.app.config['API_URI_VERSION']), data=self.testbcodes[0])
        data = json.loads(res.data.decode('utf8'))
        self.assertTrue(set(self.testbcodes[0].items()).issubset(set(data.items())))
        self.assertIn("200", res.status)

    def test_1301_recreate_barcode(self):
        res = self.app.post('/{}/barcodes/'.format(paymate.app.config['API_URI_VERSION']), data=self.testbcodes[0])
        self.assertIn("400", res.status)

    def test_1400_create_invalid_barcode(self):
        res = self.app.post('/{}/barcodes/'.format(paymate.app.config['API_URI_VERSION']), data=self.testbcodes[1])
        self.assertIn("400", res.status)

    def test_1500_create_2nd_barcode_product(self):
        res = self.app.post('/{}/barcodes/'.format(paymate.app.config['API_URI_VERSION']), data=self.testbcodes[2])
        data = json.loads(res.data.decode('utf8'))
        self.assertTrue(set(self.testbcodes[2].items()).issubset(set(data.items())))
        self.assertIn("200", res.status)

    def test_9900_remove_barcode1(self):
        res = self.app.delete('/{}/barcodes/1/'.format(paymate.app.config['API_URI_VERSION']))
        self.assertIn("204", res.status)

    def test_9901_remove_user_with_barcode_transactions_and_image(self):
        res_user1 = self.app.delete('/{}/users/1/'.format(paymate.app.config['API_URI_VERSION']))
        res_user2 = self.app.delete('/{}/users/2/'.format(paymate.app.config['API_URI_VERSION']))
        self.assertIn("204", res_user1.status)
        self.assertIn("204", res_user2.status)

    def test_9902_remove_product_with_barcode_transactions_and_image(self):
        res_product1 = self.app.delete('/{}/products/1/'.format(paymate.app.config['API_URI_VERSION']))
        res_product2 = self.app.delete('/{}/products/2/'.format(paymate.app.config['API_URI_VERSION']))
        self.assertIn("204", res_product1.status)
        self.assertIn("204", res_product2.status)

    def test_9999_check_cleanup(self):
        res_user = self.app.get('/{}/users/'.format(paymate.app.config['API_URI_VERSION']))
        data = json.loads(res_user.data.decode('utf8'))
        self.assertTrue("200" in res_user.status and data == [])


    # TODO:
    # - modify product
    # - add image
    # - assign image to user
    # - assign image to product
    # - assign image to denomination
    # - delete image
    # - buy product via barcode
    # - select user via barcode


if __name__ == '__main__':
    unittest.main()

