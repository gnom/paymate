#!/usr/bin/env python3
from gevent.pywsgi import WSGIServer
from app import *


if __name__ == '__main__':
    if app.config['DEBUG']:
        app.run(host=app.config['HOST'], port=app.config['PORT'])
    else:
        app_server = WSGIServer((app.config['HOST'], app.config['PORT']), app)
        app_server.serve_forever()
