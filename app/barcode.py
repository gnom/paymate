class Barcode():
    """ Provides methods to deal with GTIN barcodes """

    def check(barcode_string):
        """
        Checks if barcode is a valid gtin8 to gtin14 barcode.

        :param barcode_string: The barcode to verify.
        """
        try:
            barcode = int(barcode_string)
        except:
            return False

        barcode_string = str(barcode)
        orig_length = len(barcode_string)
        # barcode must either be 8, 12, 13 or 14 digits long
        if orig_length not in [8, 12, 13, 14]:
            print('invalid length')
            return False

        # verfiy checksum
        # barcodes are padded with 0 up to a lenth of 14 digits
        # add all digits in odd positions are multiplied with 3
        # digits in even with 1
        # the results are summed up
        pad_barcode = barcode_string.rjust(14, '0')
        barcode_sum = 0

        # as the list indicies start with 0 even and odd numbers are shifted
        for barcode_digit in range(0, 13):
            if barcode_digit % 2 == 1:
                barcode_sum += int(pad_barcode[barcode_digit]) * 1
            else:
                barcode_sum += int(pad_barcode[barcode_digit]) * 3

        # the sum is to be subtracted from the nearest equal or higher multiple
        # of ten

        if barcode_sum % 10 == 0:
            check_digit = 0
        else:
            tenmultiple = int(barcode_sum / 10)
            tenmultiple += 1
            tenmultiple *= 10
            check_digit = tenmultiple - barcode_sum

        if int(barcode_string[-1]) == int(check_digit):
            return True
        else:
            return False
