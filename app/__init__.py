from flask_api import FlaskAPI
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_cors import CORS
import logging
try:
    import local_config as config
except:
    import config as config

# setting up app
app = FlaskAPI(__name__)
app.config.from_object(config.use_config)
app.config['MAX_CONTENT_LENGTH'] = 12 * 1024 * 1024

# setting up logging
log = logging.getLogger('paymate')
log.setLevel(app.config['LOGLEVEL'])
ch = logging.StreamHandler()
ch.setLevel(app.config['LOGLEVEL'])
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
log.addHandler(ch)

log.info('paymate started')
log.debug('Loglevel set to {}'.format(app.config['LOGLEVEL']))
log.debug('flask app initialized as "{}"'.format(app.name))

# initializing database
# TODO: Add exception if database is not running!
db = SQLAlchemy(app)
log.debug('database initialised {}'.format(db.engine))

migrate = Migrate(app, db)

CORS(app, resources=app.config['CORS_HEADERS'])
log.debug('CORS resources set to {}'.format(app.config['CORS_HEADERS']))
log.debug('CORS Headers Origin set to {}'.format(app.config['CORS_ORIGIN']))

from . import api
