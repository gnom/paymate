import pytz
import json
import uuid
import os
import magic
import dateutil.parser
from sqlalchemy.sql import func
import sqlalchemy.ext
import sqlalchemy.orm
from datetime import datetime
from flask_api import status
from . import db, helpers, app, log, barcode



class Product(db.Model):
    __tablename__ = 'products'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(64), unique=True)
    caffeine = db.Column(db.Integer, default=None)
    alcohol = db.Column(db.Integer, default=None)
    price = db.Column(db.Integer, default=150)
    created_at = db.Column(db.DateTime(timezone=True))
    updated_at = db.Column(db.DateTime(timezone=True))
    image = db.Column(db.Integer, default=None)
    active = db.Column(db.Boolean, default=True)
    stock = db.Column(db.Integer, default=None)
    energy = db.Column(db.Integer, default=None)
    sugar = db.Column(db.Integer, default=None)
    package_size = db.Column(db.String(64), default=None)

    def __repr__(self):
        if self.created_at:
            self.created_at = str(self.created_at)
        else:
            self.created_at = '1970-01-01T00:00:00Z'

        if self.updated_at:
            self.updated_at = str(self.updated_at)
        else:
            self.updated_at = '1970-01-01T00:00:00Z'
        prod = {
            "id": self.id,
            "name": self.name,
            "caffeine": self.caffeine,
            "alcohol": self.alcohol,
            "price": self.price,
            "energy": self.energy,
            "sugar": self.sugar,
            "created_at": self.created_at,
            "updated_at": self.updated_at,
            "active": self.active,
            "image": self.image,
            "stock": self.stock,
            "package_size": self.package_size,
        }
        return json.dumps(prod)

    def attributes(self):
        attrs = [
            "id",
            "name",
            "caffeine",
            "alcohol",
            "energy",
            "sugar",
            "price",
            "created_at",
            "updated_at",
            "active",
            "image",
            "stock",
            "package_size",
            ]
        return attrs

    def patchable_attributes(self):
        attrs = [
            "name",
            "caffeine",
            "alcohol",
            "energy",
            "sugar",
            "price",
            "active",
            "image",
            "stock",
            "package_size",
            ]
        return attrs

    def set_details(self, data, method):
        pa = self.patchable_attributes()
        new = False
        update = False
        if not self.id and method == 'POST':
            new = True
            log.info('creating new product')
        if new and not 'name' in data:
            log.error('product name in request missing')
            return [{'error': 'name required'}, status.HTTP_400_BAD_REQUEST]
        if method == 'PATCH':
            log.info('update product {} with id {}'.format(self.name, self.id))
        for attr, value in data.items():
            if attr in pa:
                update = True
                setattr(self, attr, value)
        if update or new:
            self.updated_at = datetime.utcnow()
        if new:
            self.created_at = datetime.utcnow()
        if new or update:
            try:
                db.session.add(self)
                db.session.commit()
            except sqlalchemy.exc.IntegrityError as e:
                db.session.rollback()
                log.error('a product  with the name {} exists already'.format(data['name']))
                return [{'error': 'name already in use'}, status.HTTP_500_INTERNAL_SERVER_ERROR]
            return [str(self), status.HTTP_200_OK]
        else:
            log.warning('no data provided to update or crate a product')
            return [{'error': 'no data to update or create a product'}, status.HTTP_400_BAD_REQUEST]

    def get_details(self):
        return [str(self), status.HTTP_200_OK]

    def delete(self):
        """
        delete product from database and set all transactions to productid Null
        :return:
        """
        log.info('delete product {} with id {}'.format(self.name, self.id))
        try:
            log.info('setting product id {} to 0 in transactions'.format(self.id))
            db.session.query(Transaction).filter(Transaction.product_id == self.id).update({Transaction.product_id: None})
            log.info('removing barcodes associated with product {}'.format(self.name))
            db.session.query(Barcode).filter(Barcode.type == 'product').filter(Barcode.linked == self.id).delete()
            db.session.query(Product).filter(Product.id == self.id).delete()
            db.session.commit()
        except BaseException as e:
            db.session.rollback()
            log.error('an error occurred while deleting product: {}'.format(e))
            return {"error": e}, status.HTTP_500_INTERNAL_SERVER_ERROR
        log.info('product {} with id {} deleted'.format(self.name, self.id))
        return ['', status.HTTP_204_NO_CONTENT]

    def get_by_barcode(barcode):
        product = Product.query.get(barcode)
        if not product:
            return [{'error': 'barcode {} not found'.format(barcode)}, status.HTTP_404_NOT_FOUND]
        if not isinstance(product, Product):
            return [{'error': 'barcode {} not associated with a product'}, status.HTTP_400_BAD_REQUEST]
        product = Product.query.get(product['id'])
        return product


class User(db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(64), unique=True)
    email = db.Column(db.String(128), unique=True)
    created_at = db.Column(db.DateTime)
    updated_at = db.Column(db.DateTime)
    balance = db.Column(db.Integer, default=0)
    active = db.Column(db.Boolean, default=True)
    audit = db.Column(db.Boolean, default=False)
    redirect = db.Column(db.Boolean, default=True)
    avatar = db.Column(db.Integer, default=None)


    def count(self):
        count = User.query.with_entities(func.count(User.id).label('count')).first()
        return count[0]

    def count_active(self):
        active = User.query.with_entities(func.count(User.id).label('count')).filter(User.active).first()
        return active[0]

    def __repr__(self):
        if self.created_at:
            self.created_at = str(self.created_at)
        else:
            self.created_at = '1970-01-01T00:00:00Z'

        if self.updated_at:
            self.updated_at = str(self.updated_at)
        else:
            self.updated_at = '1970-01-01T00:00:00Z'

        user = {
            "id": self.id,
            "name": self.name,
            "email": self.email,
            "created_at": self.created_at,
            "updated_at": self.updated_at,
            "balance": self.balance,
            "active": self.active,
            "audit": self.audit,
            "redirect": self.redirect,
            "avatar": self.avatar
        }
        return json.dumps(user)

    def attributes(self):
        attrs = [
            "id",
            "name",
            "email",
            "created_at",
            "updated_at",
            "balance",
            "active",
            "audit",
            "redirect",
            "avatar"
        ]
        return attrs

    def patchable_attributes(self):
        attrs = [
            "name",
            "email",
            "active",
            "audit",
            "redirect",
            "avatar"
            ]
        return attrs

    def set_details(self, data, method):
        """
        Upates a user record
        :param kwargs: dict of attributes / values to add / update
        :return: True on success, else an error message
        """
        pa = self.patchable_attributes()
        new = False
        update = False
        if not self.id and method == 'POST':
            new = True
            log.info('creating new user')
        if new and not 'name' in data:
            log.error('username in request missing')
            return [{'error': 'name required'}, status.HTTP_400_BAD_REQUEST]
        if method == 'PATCH':
            log.info('update user {} with id {}'.format(self.name, self.id))
        for attr, value in data.items():
            if attr in pa:
                update = True
                log.debug('{}, {}'.format(attr, value))
                setattr(self, attr, value)
        if update or new:
            self.updated_at = datetime.utcnow()
        if new:
            self.created_at = datetime.utcnow()
        if new or update:
            try:
                db.session.add(self)
                db.session.commit()
            except sqlalchemy.exc.IntegrityError as e:
                db.session.rollback()
                log.error('username {} already in use'.format(data['name']))
                return [{'error': 'username already in use'}, status.HTTP_500_INTERNAL_SERVER_ERROR]
            return [str(self), status.HTTP_200_OK]
        else:
            log.warning('no data provided to update or crate an user')
            return [{'error': 'no data to update or create an user'}, status.HTTP_400_BAD_REQUEST]

    def get_details(self):
        """
        Get all details for a given user
        :param uid: id of the user
        """
        return [str(self), status.HTTP_200_OK]

    def spend_deposit(self, type, data):
        """
        create a transaction without buying anything e.g. recharging your account or transferring money to an other user
        :param type: foo
        :param data: request data from user/id/spend or user/id/deposit
        :return: empty string on success else an error is 
        """
        if not self.id:
            return [{'error': 'no user selected'}, status.HTTP_400_BAD_REQUEST]
        if 'amount' not in data:
            return [{'error': 'amount not specified'}, status.HTTP_400_BAD_REQUEST]
        trans = Transaction()
        try:
            amount = int(data['amount'])
        except:
            return [{'error': 'amount has to be an integer'}, status.HTTP_400_BAD_REQUEST]
        if type == 'deposit':
            if self.balance + amount >= app.config['GLOBAL_BUDGET_LIMIT']:
                return [{'error': 'you have reached the maximum credit limit'}, status.HTTP_400_BAD_REQUEST]
            self.balance += amount
        elif type == 'spend':
            if not self.balance + app.config['GLOBAL_CREDIT_LIMIT'] >= amount:
                return [{'error': 'insufficient funds on your account'}, status.HTTP_402_PAYMENT_REQUIRED]
            self.balance -= amount
        else:
            return [{'error': 'invalid transaction type'}, status.HTTP_400_BAD_REQUEST]
        if not helpers.check_integer(data['amount'], min=1):
            return [{'error': 'amount has to be greater than 0'}, status.HTTP_400_BAD_REQUEST]
        trans.action = type
        trans.amount = data['amount']
        trans.created_at = datetime.utcnow()
        trans.user_id = self.id
        self.updated_at = datetime.utcnow()
        try:
            db.session.add(self)
            db.session.add(trans)
            db.session.commit()
        except:
            db.session.rollback()
            return [{'error': 'database error'}, status.HTTP_500_INTERNAL_SERVER_ERROR]
        return ['', status.HTTP_204_NO_CONTENT]

    def buy_product(self, data):
        if 'product' in data:
            product = Product.query.get(data['product'])
            if not product:
                return [{'error': 'product not found'}, status.HTTP_400_BAD_REQUEST]
        elif 'barcode' in data:
            bcode = Barcode.query.filter_by(barcode=data['barcode']).first()
            if not bcode:
                return [{'error': 'barcode not found'}, status.HTTP_400_BAD_REQUEST]
            if bcode.type != 'product':
                return [{'error': 'barcode not associated with a product'}, status.HTTP_400_BAD_REQUEST]
            product = Product.query.get(bcode.linked)
            if not product:
                return [{'error': 'no product associated with barcode'}, status.HTTP_400_BAD_REQUEST]
        else:
            return [{'error': 'product or barcode missing in request'}, status.HTTP_400_BAD_REQUEST]
        if not self.balance + app.config['GLOBAL_CREDIT_LIMIT'] >= product.price:
            return [{'error': 'insufficient funds on your account'}, status.HTTP_402_PAYMENT_REQUIRED]
        trans = Transaction()
        trans.amount = product.price
        trans.action = 'spend'
        trans.product_id = product.id
        if self.audit:
            trans.user_id = self.id
        trans.created_at = datetime.utcnow()
        self.balance -= product.price
        self.updated_at = datetime.utcnow()
        db.session.add(self)
        db.session.add(trans)
        try:
            db.session.commit()
        except BaseException as e:
            return [{'error': e}, status.HTTP_500_INTERNAL_SERVER_ERROR]

        return ['', status.HTTP_204_NO_CONTENT]

    def get_stats(self):
        user = User()
        trans = Transaction()
        if not user or not trans:
            return [{'error': 'something went terrible wrong!'}, status.HTTP_500_INTERNAL_SERVER_ERROR]
        data = {'user_count': int(user.count()),
                'active_count': int(user.count_active()),
                'balance_sum': int(trans.balance_sum())
                }
        return [data, status.HTTP_200_OK]

    def delete(self):
        """
        delete user from database and set all transactions to userid Null
        :return:
        """
        log.info('delete user {} with id {}'.format(self.name, self.id))
        try:
            log.info('setting user id {} to 0 in transactions'.format(self.id))
            db.session.query(Transaction).filter(Transaction.user_id == self.id).update({Transaction.user_id: None})
            log.info('removing barcodes associated with user {}'.format(self.name))
            db.session.query(Barcode).filter(Barcode.type == 'user').filter(Barcode.linked == self.id).delete()
            db.session.query(User).filter(User.id == self.id).delete()
            db.session.commit()
        except BaseException as e:
            db.session.rollback()
            log.error('an error occurred while deleting user: {}'.format(e))
            return {"error": e}, status.HTTP_500_INTERNAL_SERVER_ERROR
        log.info('user {} with id {} deleted'.format(self.name, self.id))
        return ['', status.HTTP_204_NO_CONTENT]

    def transfer(self, data):
        """
        transfer funds from one account to another one
        :param data: request data
        :return:
        """
        if not 'receiver' in data and not 'amount':
            return [{'error': 'receiver and amount are required in the request'}, status.HTTP_400_BAD_REQUEST]
        try:
            data['amount'] = int(data['amount'])
        except:
            return [{'error': 'amount has to be an integer value'}, status.HTTP_400_BAD_REQUEST]
        if not self.balance + app.config['GLOBAL_CREDIT_LIMIT'] >= data['amount']:
            return [{'error': 'insufficient funds on senders account'}, status.HTTP_402_PAYMENT_REQUIRED]
        recv = User.query.get(data['receiver'])
        if not recv:
           return [{'error': 'receiver not fount'}, status.HTTP_400_BAD_REQUEST]
        recvReturn = recv.spend_deposit('deposit', data)
        if not recvReturn[1] == 204:
            return recvReturn
        spendReturn = self.spend_deposit('spend', data)
        if not 204 in spendReturn:
            return spendReturn
        return ['', 204]

    def get_by_barcode(barcode):
        """
        return user by given barcode
        :param barcode: barcode to get user for
        :return: user associated with barcode
        """
        bcode = Barcode.query.filter(Barcode.barcode == barcode).filter(Barcode.type == 'user').first()
        if not bcode:
            return [{'error': 'barcode {} not found'.format(barcode)}, status.HTTP_404_NOT_FOUND]
        user = User.query.get(bcode.id)
        if not isinstance(user, User):
            return [{'error': 'barcode {} not associated with an user'}, status.HTTP_400_BAD_REQUEST]
        return [str(user), status.HTTP_200_OK]


class Transaction(db.Model):
    __tablename__ = 'transactions'
    id = db.Column(db.Integer, primary_key=True)
    action = db.Column(db.String(32))
    amount = db.Column(db.Integer)
    created_at = db.Column(db.DateTime)
    product_id = db.Column(db.Integer, db.ForeignKey('products.id'))
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))

    def balance_sum(self):
        balance = User.query.with_entities(func.sum(User.balance).label('balance_sum')).first()
        return balance[0]

    def __repr__(self):
        if self.created_at:
            self.created_at = str(self.created_at)
        else:
            self.created_at = '1970-01-01T00:00:00Z'

        transaction = {
            "id": self.id,
            "action": self.action,
            "amount": self.amount,
            "created_at": self.created_at,
            "product_id": self.product_id,
            "user_id": self.user_id
        }
        return json.dumps(transaction)

    def attributes(self):
        attrs = [
            "id",
            "action",
            "amount",
            "created_at",
            "product_id",
            "user_id"
        ]
        return attrs

    def audit(args):
        log.debug('arguments passed to function: {}'.format(str(args)))
        if not args:
            return [{"error": "invalid arguments"}, status.HTTP_500_INTERNAL_SERVER_ERROR]
        if 'start' not in args:
            return [{"error": "startdate is required"}, status.HTTP_400_BAD_REQUEST]
        log.debug('startdate: {}'.format(args['start']))
        if 'end' not in args:
            enddate = datetime.now()
            log.debug('setting enddate to {}'.format(enddate))
        else:
            enddate = dateutil.parser.parse(args['end'])

        startdate = dateutil.parser.parse(args['start'])

        if enddate <= startdate:
            log.error("start must be before end")
            return [{"error": "start must be before end"}, status.HTTP_400_BAD_REQUEST]

        if 'user' in args:
            user = User.query.get(args['user'])
            if not user:
                return [{"error": "user not found"}, status.HTTP_404_NOT_FOUND]
            if not user.audit:
                return [{"error": "audits deactivated by user"}, status.HTTP_401_UNAUTHORIZED]
            uid = user.id

            audit_deposit = Transaction.query.filter(
                Transaction.user_id == uid). \
                filter(Transaction.action == 'deposit'). \
                filter(Transaction.created_at >= str(startdate)). \
                filter(Transaction.created_at <= str(enddate)). \
                with_entities(func.sum(Transaction.amount)). \
                first()
        else:
            audit_deposit = Transaction.query.filter(
                Transaction.action == 'deposit'). \
                filter(Transaction.created_at >= str(startdate)). \
                filter(Transaction.created_at <= str(enddate)). \
                with_entities(func.sum(Transaction.amount)). \
                first()

        audit_deposit = audit_deposit[0]
        if not audit_deposit:
            audit_deposit = 0

        if 'user' in args:
            audit_spend = Transaction.query.filter(
                Transaction.user_id == uid). \
                filter(Transaction.action == 'spend'). \
                filter(Transaction.created_at >= str(startdate)). \
                filter(Transaction.created_at <= str(enddate)). \
                with_entities(func.sum(Transaction.amount)). \
                first()
        else:
            audit_spend = Transaction.query.filter(
                Transaction.action == 'spend'). \
                filter(Transaction.created_at >= str(startdate)). \
                filter(Transaction.created_at <= str(enddate)). \
                with_entities(func.sum(Transaction.amount)). \
                first()

        audit_spend = audit_spend[0]
        if not audit_spend:
            audit_spend = 0
        audit_sum = audit_deposit + audit_spend
        AuditData = {}
        AuditData['sum'] = int(audit_sum)
        AuditData['payments_sum'] = int(audit_spend)
        AuditData['deposits_sum'] = int(audit_deposit)
        if 'user' in args:
            audits = Transaction.query.filter(Transaction.user_id == uid). \
                filter(Transaction.created_at >= str(startdate)). \
                filter(Transaction.created_at <= str(enddate)). \
                order_by(Transaction.created_at.desc())
        else:
            audits = Transaction.query.filter(Transaction.created_at >= str(startdate)). \
                filter(enddate >= Transaction.created_at). \
                order_by(Transaction.created_at.desc())

        AuditData['audits'] = []
        for audit in audits:
            AuditData['audits'].append(json.loads(str(audit)))
        return [AuditData, status.HTTP_200_OK]


class Image(db.Model):
    __tablename__ = 'images'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    filename = db.Column(db.String(64), unique=True)
    created_at = db.Column(db.DateTime)

    def __repr__(self):
        if self.created_at:
            self.created_at = str(self.created_at)
        else:
            self.created_at = '1970-01-01T00:00:00Z'
        image = {
            "id": self.id,
            "filename": self.filename,
            "created_at": self.created_at
        }
        return json.dumps(image)

    def save_image(files):
        """
        save an uploaded image to disk
        :param files: request.files from uploading image in post request
        :return: image object and status 200 on success
        """
        if 'image' not in files:
            return [{"error": "image is required"}, status.HTTP_400_BAD_REQUEST]
        image = files['image']
        if image.filename == '':
            return [{"error": "image is required"}, status.HTTP_400_BAD_REQUEST]
        fname = str(uuid.uuid4())
        log.debug(image.filename)
        filename = os.path.join(app.config['UPLOAD_DIR'], image.filename)
        log.debug(filename)
        image.save(filename)
        mtype = magic.from_file(filename, mime=True)
        mimetypes = app.config['MIMETYPES']
        if mtype not in mimetypes.keys():
            os.remove(filename)
            return [{"error": "only {} are allowed".format(', '.join(mimetypes.values()))},
                    status.HTTP_415_UNSUPPORTED_MEDIA_TYPE]
        fname += '.png'
        fpath = os.path.join(app.config['UPLOAD_DIR'], fname)
        helpers.make_image_squared(filename, fpath)
        log.debug(fname, fpath)
        img = Image()
        img.filename = fname

        img.created_at = datetime.utcnow()
        db.session.add(img)
        db.session.commit()
        image = str(Image.query.get(img.id))
        return [image, status.HTTP_200_OK]

    def delete(self):
        """
        deletes the image from filesystem and removes all connections to products, denominations or users
        :return: 204 on success, 500 on error
        """
        log.info('delete image {} with id {}'.format(self.filename, self.id))
        try:
            log.info('removing image id {} from users'.format(self.id))
            db.session.query(User).filter(User.avatar == self.id).update({User.avatar: None})
            db.session.query(Product).filter(Product.image == self.id).update({Product.image: None})
            db.session.query(Denomination).filter(Denomination.image == self.id).update({Denomination.image: None})
            db.session.query(Image).filter(Image.id == self.id).delete()
            db.session.commit()
        except BaseException as e:
            db.session.rollback()
            log.error('an error occurred while deleting image: {}'.format(e))
            return [{"error": e}, status.HTTP_500_INTERNAL_SERVER_ERROR]
        log.info('image {} with id {} deleted from database'.format(self.filename, self.id))
        try:
            log.info('removing image {} from filesystem'.format(self.filename))
            os.remove(os.path.join(app.config['UPLOAD_DIR'], self.filename))
        except:
            log.warning('unable to delete image {} from filesystem.'.format(self.filename))
            return [{'error': 'could not delete file {} from filesystem'.format(self.filename)}, status.HTTP_500_INTERNAL_SERVER_ERROR]
        return ['', status.HTTP_204_NO_CONTENT]


class Denomination(db.Model):
    __tablename__ = 'denominations'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    amount = db.Column(db.Integer, unique=True)
    image = db.Column(db.Integer, db.ForeignKey('images.id'), default=None)
    created_at = db.Column(db.DateTime)
    updated_at = db.Column(db.DateTime)

    def __repr__(self):
        if self.created_at:
            self.created_at = str(self.created_at)
        else:
            self.created_at = '1970-01-01T00:00:00Z'

        if self.updated_at:
            self.updated_at = str(self.updated_at)
        else:
            self.updated_at = '1970-01-01T00:00:00Z'

        denomination = {
            "id": self.id,
            "amount": self.amount,
            "image": self.image,
            "created_at": self.created_at,
            "updated_at": self.updated_at,
        }
        return json.dumps(denomination)

    def attributes(self):
        attrs = [
            "id",
            "amount",
            "image",
            "created_at",
            "updated_at",
        ]
        return attrs

    def patchable_attributes(self):
        attrs = [
            "amount",
            "image",
        ]
        return attrs

    def set_details(self, data, method):
        pa = self.patchable_attributes()
        new = False
        update = False
        if not self.id and method == 'POST':
            new = True
            log.info('creating new denomination')
        if new and not 'amount' in data:
            log.error('amount of denomination missing in request')
            return [{'error': 'amount required'}, status.HTTP_400_BAD_REQUEST]
        if method == 'PATCH':
            log.info('update denomination {} with id {}'.format(self.amount, self.id))
        for attr, value in data.items():
            if attr in pa:
                update = True
                setattr(self, attr, value)
        if update or new:
            self.updated_at = datetime.utcnow()
        if new:
            self.created_at = datetime.utcnow()
        if new or update:
            try:
                db.session.add(self)
                db.session.commit()
            except sqlalchemy.exc.IntegrityError as e:
                db.session.rollback()
                log.error('a denomination with the amount {} exists already'.format(data['amount']))
                return [{'error': 'denomination exists already'}, status.HTTP_500_INTERNAL_SERVER_ERROR]
            return [str(self), status.HTTP_200_OK]
        else:
            log.warning('no data provided to update or crate a denomination')
            return [{'error': 'no data to update or create a denomination'}, status.HTTP_400_BAD_REQUEST]

    def get_details(self):
        return [str(self), status.HTTP_200_OK]

    def delete(self):
        """
        delete denomination from database
        :return:
        """
        log.info('delete denomination {} with id {}'.format(self.amount, self.id))
        try:
            db.session.query(Denomination).filter(Denomination.id == self.id).delete()
            db.session.commit()
        except BaseException as e:
            db.session.rollback()
            log.error('an error occurred while deleting denomination: {}'.format(e))
            return {"error": e}, status.HTTP_500_INTERNAL_SERVER_ERROR
        log.info('denomination {} with amount {} deleted'.format(self.amount, self.id))
        return ['', status.HTTP_204_NO_CONTENT]


class Barcode(db.Model):
    __tablename__ = 'barcodes'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    barcode = db.Column(db.String(32), unique=True)
    type = db.Column(db.String(32))
    linked = db.Column(db.Integer)
    created_at = db.Column(db.DateTime)
    updated_at = db.Column(db.DateTime)

    def __repr__(self):
        if self.created_at:
            self.created_at = str(self.created_at)
        else:
            self.created_at = '1970-01-01T00:00:00Z'

        if self.updated_at:
            self.updated_at = str(self.updated_at)
        else:
            self.updated_at = '1970-01-01T00:00:00Z'

        barcode = {
            "id": self.id,
            "barcode": self.barcode,
            "type": self.type,
            "linked": self.linked,
            "created_at": self.created_at,
            "updated_at": self.updated_at,
        }
        return json.dumps(barcode)

    def patchable_attributes(self):
        attrs = [
            "barcode",
            "type",
            "linked",
        ]
        return attrs

    def attributes(self):
        attrs = [
            "id",
            "barcode",
            "type",
            "linked",
            "created_at",
            "updated_at",
        ]
        return attrs

    def create_barcode(self, data):
        if 'barcode' not in data:
            return [{'error': 'barcode is required'}, status.HTTP_400_BAD_REQUEST]

        if not barcode.Barcode.check(data['barcode']):
            return [{'error': 'invalid barcode. Barcode must be within gtin8 to gtin 14'}, status.HTTP_400_BAD_REQUEST]

        if 'type' not in data:
            return [{'error': 'type is required'}, status.HTTP_400_BAD_REQUEST]

        if data['type'] not in ['user', 'product']:
            return [{'error': 'type has to be user or product'}, status.HTTP_400_BAD_REQUEST]

        if 'linked' not in data:
            return [{'error': 'linked is required'}, status.HTTP_400_BAD_REQUEST]

        if data['type'] == 'product':
            linked = Product.query.get(data['linked'])
        else:
            linked = User.query.get(data['linked'])

        if linked is None:
            log.info('{} id not found ({})'.format(data['type'], data['linked']))
            return [{'error': '{} id not found ({})'.format(data['type'], data['linked'])}, status.HTTP_400_BAD_REQUEST]

        pa = self.patchable_attributes()
        for attr, value in data.items():
            if attr in pa:
                setattr(self, attr, value)

        self.created_at = datetime.utcnow()
        self.updated_at = datetime.utcnow()

        try:
            db.session.add(self)
            db.session.commit()
        except sqlalchemy.exc.IntegrityError:
            db.session.rollback()
            return [{'error': 'this barcode exists already'}, status.HTTP_400_BAD_REQUEST]
        except:
            return [{'error': 'something went terribly wrong'}, status.HTTP_500_INTERNAL_SERVER_ERROR]

        return [str(self), status.HTTP_200_OK]

    def update_barcode(self, data):
        update = False
        if 'barcode' in data:
            update = True
            if not barcode.Barcode.check(data['barcode']):
                return [{'error': 'barcode "{}" does not match gtin8 to gtin14 specifications'.format(data['barcode'])}, status.HTTP_400_BAD_REQUEST]
            self.barcode = data['barcode']
        if 'type' in data and 'linked' in data:
            update = True
            if data['type'] not in ['user', 'product']:
                return [{'error': 'type must be user or product'}, status.HTTP_400_BAD_REQUEST]
            if data['type'] == "user":
                linked = User.query.get(data['linked'])
            else:
                linked = Product.query.get(data['linked'])
            if not linked:
                return [{'error': 'no {} with id {}'}.format(data['type'], data['linked']), status.HTTP_400_BAD_REQUEST]
            self.type = data['type']
            self.linked = data['linked']

        if not update:
            return [{'error': 'invalid request'}, status.HTTP_400_BAD_REQUEST]

        self.updated_at = datetime.utcnow()

        try:
            db.session.add(self)
            db.session.commit()
        except:
            db.session.rollback()
            return [{'error': 'something in the database was wibbly wobbly timey wimey.'}, status.HTTP_500_INTERNAL_SERVER_ERROR]

        return [str(self), status.HTTP_200_OK]

    def delete(self):
        db.session.query(Barcode).filter(Barcode.id == self.id).delete()
        try:
            db.session.commit()
        except:
            db.session.rollback()
            return [{'error': 'something in the database went wrong'}, status.HTTP_500_INTERNAL_SERVER_ERROR]
        return ['', status.HTTP_204_NO_CONTENT]
