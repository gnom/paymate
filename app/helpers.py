from PIL import Image
import os
from . import app, log


def check_integer(value, min=False, max=False):
    try:
        value = int(value)
        if min:
            min = int(min)
        if max:
            max = int(max)
    except:
        return False
    if min and not max and value < min:
        return False
    elif not min and max and value > max:
        return False
    elif min and max and not value >= min and not value <= max:
        return False
    else:
        return True


def make_image_squared(path_to_img, name):
    log.debug(name)
    img = Image.open(path_to_img, 'r')
    newimg = Image.new('RGBA', (500, 500), (255, 255, 255, 0))
    dimension = img.size
    log.debug(dimension)
    if max(dimension) > 500:
        log.debug('Image is bigger than 500x500')
        img.thumbnail((500, 500))
        dimension = img.size
        log.debug(dimension)
        if dimension[0] > dimension[1]:
            offset = (0, (500-dimension[1]) // 2)
        else:
            offset = ((500-dimension[0]) // 2, 0)
        newimg.paste(img, offset)
    else:
        offset = ((500 - dimension[0]) // 2, (500 - dimension[1]) // 2)
        newimg.paste(img, offset)
    newimg.save(name)
    os.remove(path_to_img)
