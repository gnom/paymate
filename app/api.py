import json
import magic
import os
from flask import jsonify, request, abort
from flask import send_file
from flask_api import status
from . import app, db, log
from .models import Product, User, Transaction, Image, Denomination, Barcode


@app.route('/{}/info/'.format(app.config['API_URI_VERSION']), methods=['GET'])
@app.route('/{}/'.format(app.config['API_URI_VERSION']), methods=['GET'])
def api_info():
    data = {'api_version': app.config['API_VERSION'],
            'global_credit_limit': app.config['GLOBAL_CREDIT_LIMIT'],
            'currency': app.config['CURRENCY'],
            'currency_before': app.config['CURRENCY_BEFORE'],
            'decimal_separator': app.config['DECIMAL_SEPARATOR'],
            'capabilities': ['image'],
            'denominations': app.config['DENOMINATIONS'],
            'energy': 'kcal',
            }
    return jsonify(data)


# Products
@app.route('/{}/products/'.format(app.config['API_URI_VERSION']), methods=['GET'])
def api_products_get():
    products = Product.query.order_by('name').all()
    if not products:
        return [{'error': 'no products found'}, status.HTTP_404_NOT_FOUND]
    data = []
    for prod in products:
        data.append(json.loads(str(prod)))
    return [data, status.HTTP_200_OK]


@app.route('/{}/products/'.format(app.config['API_URI_VERSION']), methods=['POST'])
def api_products_post():
    prod = Product()
    addResponse = prod.set_details(request.data, request.method)
    log.debug(request.method)
    return addResponse[0], addResponse[1]


@app.route('/{}/products/<int:prod_id>/'.format(app.config['API_URI_VERSION']), methods=['GET'])
def api_products_id_get(prod_id):
    data = Product.query.get(prod_id)
    if not data:
        return [{'error': 'product not found'}, status.HTTP_404_NOT_FOUND]
    returnProduct = data.get_details()
    return returnProduct[0], returnProduct[1]


@app.route('/{}/products/<int:prod_id>/'.format(app.config['API_URI_VERSION']), methods=['PATCH'])
def api_products_id_patch(prod_id):
    prod = Product.query.get(prod_id)
    if not prod:
        return {"error": "product not found"}, status.HTTP_404_NOT_FOUND
    updateResponse = prod.set_details(request.data, request.method)
    return updateResponse[0], updateResponse[1]


@app.route('/{}/products/<int:prod_id>/'.format(app.config['API_URI_VERSION']), methods=['DELETE'])
def api_products_id_delete(prod_id):
    prod = Product.query.get(prod_id)
    if not prod:
        return {'error': 'product not found'}, status.HTTP_404_NOT_FOUND
    deleteResponse = prod.delete()
    return deleteResponse[0], deleteResponse[1]


# Users
@app.route('/{}/users/'.format(app.config['API_URI_VERSION']), methods=['GET'])
def api_user_get():
    users = User.query.order_by('name').all()
    data = []
    for user in users:
        data.append(json.loads(str(user)))
    return data


@app.route('/{}/users/'.format(app.config['API_URI_VERSION']), methods=['POST'])
def api_user_post():
    user = User()
    addResponse = user.set_details(request.data, request.method)
    log.debug(request.method)
    return addResponse[0], addResponse[1]


@app.route('/{}/users/<int:user_id>/'.format(app.config['API_URI_VERSION']), methods=['GET'])
def api_user_id_get(user_id):
    data = User.query.get(user_id)
    if not data:
        return [{'error': 'user not found'}, status.HTTP_404_NOT_FOUND]
    returnUser = data.get_details()
    return returnUser[0], returnUser[1]


@app.route('/{}/users/<int:user_id>/'.format(app.config['API_URI_VERSION']), methods=['PATCH'])
def api_user_id_patch(user_id):
    user = User.query.get(user_id)
    if not user:
        return {"error": "user not found"}, status.HTTP_404_NOT_FOUND
    updateResponse = user.set_details(request.data, request.method)
    return updateResponse[0], updateResponse[1]


@app.route('/{}/users/<int:user_id>/'.format(app.config['API_URI_VERSION']), methods=['DELETE'])
def api_user_id_delete(user_id):
    user = User.query.get(user_id)
    if not user:
        return {'error': 'user not found'}, status.HTTP_404_NOT_FOUND
    deleteResponse = user.delete()
    return deleteResponse[0], deleteResponse[1]


@app.route('/{}/users/<int:user_id>/deposit/'.format(app.config['API_URI_VERSION']), methods=['POST'])
def api_user_id_deposit_post(user_id):
    log.debug(request.data['amount'])
    user = User.query.get(user_id)
    if not user:
        return {'error': 'user not found'}, status.HTTP_404_NOT_FOUND
    sdReturn = user.spend_deposit('deposit', request.data)
    
    return sdReturn[0], sdReturn[1]


@app.route('/{}/users/<int:user_id>/spend/'.format(app.config['API_URI_VERSION']), methods=['POST'])
def api_user_id_spend_post(user_id):
    user = User.query.get(user_id)
    if not user:
        return {'error': 'user not found'}, status.HTTP_404_NOT_FOUND
    sdReturn = user.spend_deposit('spend', request.data)
    return sdReturn[0], sdReturn[1]


@app.route('/{}/teapot/'.format(app.config['API_URI_VERSION']), methods=['GET'])
def api_user_teapot_get():
    return {'error': 'I\'m a teapot, not a coffee machine'}, 418


@app.route('/{}/users/<int:user_id>/buy/'.format(app.config['API_URI_VERSION']), methods=['POST'])
@app.route('/{}/users/<int:user_id>/buy/barcode/'.format(app.config['API_URI_VERSION']), methods=['POST'])
def api_user_id_buy_post(user_id):
    user = User.query.get(user_id)
    if not user:
        return {'error': 'user not found'}, status.HTTP_404_NOT_FOUND
    bpReturn = user.buy_product(request.data)
    print(bpReturn)
    return bpReturn[0], bpReturn[1]


@app.route('/{}/users/<int:user_id>/transfer/'.format(app.config['API_URI_VERSION']), methods=['POST'])
def api_user_id_transfer_post(user_id):
    user = User.query.get(user_id)
    if not user:
        return {'error': 'user not found'}, status.HTTP_404_NOT_FOUND
    transReturn = user.transfer(request.data)
    return transReturn[0], transReturn[1]


@app.route('/{}/users/barcode/<barcode>/'.format(app.config['API_URI_VERSION']))
def api_user_barcode_barcode_get(barcode):
    bcode = User.get_by_barcode(barcode)
    return bcode[0], bcode[1]


@app.route('/{}/users/stats/'.format(app.config['API_URI_VERSION']), methods=['GET'])
def api_users_stats_get():
    user = User()
    stats = user.get_stats()
    return stats[0], stats[1]


# Images
@app.route('/{}/images/'.format(app.config['API_URI_VERSION']), methods=['GET'])
def api_images_get():
    return '', status.HTTP_204_NO_CONTENT


@app.route('/{}/images/'.format(app.config['API_URI_VERSION']), methods=['POST'])
def api_images_post():
    print(request.files)
    response = Image.save_image(request.files)
    return response[0], response[1]


@app.route('/{}/images/<int:img_id>/'.format(app.config['API_URI_VERSION']), methods=['GET'])
def api_images_id_get(img_id):
    img = Image.query.get(img_id)
    if not img:
        return {"error": "image not found"}, status.HTTP_404_NOT_FOUND
    return json.loads(str(img))


@app.route('/{}/images/<int:img_id>/'.format(app.config['API_URI_VERSION']), methods=['DELETE'])
def api_images_id_delete(img_id):
    img = Image.query.get(img_id)
    if not img:
        return {"error": "image not found"}, status.HTTP_404_NOT_FOUND
    response = img.delete()
    return response[0], response[1]


@app.route('/{}/images/<int:img_id>/img/'.format(app.config['API_URI_VERSION']), methods=['GET'])
def api_images_id_img_get(img_id):
    # TODO: refactor image get
    img = Image.query.get(img_id)
    if not img:
        return {"error": "image not found"}, status.HTTP_404_NOT_FOUND
    filename = os.path.join(
        app.config['BASEDIR'],
        app.config['UPLOAD_DIR'],
        img.filename
    )
    mimetype = magic.from_file(filename)
    return send_file(
        filename,
        mimetype=mimetype,
        attachment_filename=img.filename,
        as_attachment=True
    )


# Audits
@app.route('/{}/audits/'.format(app.config['API_URI_VERSION']), methods=['GET'])
def api_audits_get():
    audit = Transaction.audit(request.args)
    return audit[0], audit[1]


# Denominations
@app.route('/{}/denominations/'.format(app.config['API_URI_VERSION']), methods=['GET'])
def api_denominations_get():
    denoms = Denomination.query.order_by('amount').all()
    data = []
    for denom in denoms:
        data.append(json.loads(str(denom)))
    return data


@app.route('/{}/denominations/'.format(app.config['API_URI_VERSION']), methods=['POST'])
def api_denominations_post():
    denom = Denomination()
    addResponse = denom.set_details(request.data, request.method)
    return addResponse[0], addResponse[1]


@app.route('/{}/denominations/<int:denom_id>/'.format(app.config['API_URI_VERSION']), methods=['GET'])
def api_denominations_id_get(denom_id):
    data = Denomination.query.get(denom_id)
    if not data:
        return [{'error': 'denomination not found'}, status.HTTP_404_NOT_FOUND]
    response = data.get_details()
    return response[0], response[1]


@app.route('/{}/denominations/<int:denom_id>/'.format(app.config['API_URI_VERSION']), methods=['PATCH'])
def api_denominations_id_patch(denom_id):
    denom = Denomination.query.get(denom_id)
    if not denom:
        return {"error": "Denomination not found"}, status.HTTP_404_NOT_FOUND
    updateResponse = denom.set_details(request.data, request.method)
    return updateResponse[0], updateResponse[1]


@app.route('/{}/denominations/<int:denom_id>/'.format(app.config['API_URI_VERSION']), methods=['DELETE'])
def api_denominations_id_delete(denom_id):
    denom = Denomination.query.get(denom_id)
    if not denom:
        return {'error': 'Denomination not found'}, status.HTTP_404_NOT_FOUND
    deleteResponse = denom.delete()
    return deleteResponse[0], deleteResponse[1]


## barcodes
@app.route('/{}/barcodes/'.format(app.config['API_URI_VERSION']), methods=['POST'])
def api_barcodes_post():
    barcode = Barcode()
    response = barcode.create_barcode(request.data)
    return response[0], response[1]


@app.route('/{}/barcodes/<int:bcode_id>/'.format(app.config['API_URI_VERSION']), methods=['GET'])
def api_barcode_id_get(bcode_id):
    barcode = Barcode.query.get(bcode_id)
    if not barcode:
        return {'error': 'Barcode {} not found'.format(bcode_id)}, status.HTTP_404_NOT_FOUND
    return str(barcode), status.HTTP_200_OK


@app.route('/{}/barcodes/<int:bcode_id>/'.format(app.config['API_URI_VERSION']), methods=['UPDATE'])
def api_barcode_id_update(bcode_id):
    barcode = Barcode.query.get(bcode_id)
    if not barcode:
        return {'error': 'Barcode not found'}, status.HTTP_404_NOT_FOUND
    response = barcode.update_barcode(request.data)
    return response[0], response[1]


@app.route('/{}/barcodes/<int:bcode_id>/'.format(app.config['API_URI_VERSION']), methods=['DELETE'])
def api_barcode_id_delete(bcode_id):
    barcode = Barcode.query.get(bcode_id)
    if not barcode:
        return [{'error': 'Barcode not found'}, status.HTTP_404_NOT_FOUND]
    response = barcode.delete()
    return response[0], response[1]
