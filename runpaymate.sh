#!/bin/bash
while true; do
  echo "wait for datebase to finish startup..."
  echo "quit" |  mysql -u$MYSQL_USER -p$MYSQL_PASSWORD -h db > /dev/null 2>&1
  if [ $? -eq 0 ]; then
    break
  fi
  sleep 1
done
mkdir -p /paymate/data/images
flask db upgrade
python3 paymate.py
