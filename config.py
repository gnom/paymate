# -*- coding: utf-8 -*-
import os
import sys
import logging

class Config(object):
    PORT = 2342
    HOST = '0.0.0.0'
    GLOBAL_CREDIT_LIMIT = 2000
    GLOBAL_BUDGET_LIMIT = 10000
    CURRENCY = '€'
    CURRENCY_BEFORE = False
    DECIMAL_SEPARATOR = ','

    # logging
    LOGLEVEL = logging.INFO

    # allowed mime types for images
    MIMETYPES = {
        'image/png': '.png',
        'image/jpeg': '.jpg',
        'image/gif': '.gif',
    }

    # cors
    CORS_ORIGIN = r'/*'
    CORS_HEADERS = 'Content-Type'

    # mysql configuration
    MYSQL_DATABASE_USER = 'paymate'
    MYSQL_DATABASE_PASS = 'paymate'
    MYSQL_DATABASE_NAME = 'paymate'
    MYSQL_DATABASE_HOST = 'db'

    SQLALCHEMY_DATABASE_URI = 'mysql://{}:{}@{}/{}'.format(MYSQL_DATABASE_USER, MYSQL_DATABASE_PASS, MYSQL_DATABASE_HOST, MYSQL_DATABASE_NAME)

    # denominations
    DENOMINATIONS = [
        100,
        200,
        500,
        1000,
        2000,
        5000
    ]

    UPLOAD_DIR = 'data/images'
    # ---------------------------------------
    # do not change anything below this line!
    # ---------------------------------------
    BASEDIR = os.path.abspath(os.path.dirname(__name__))
    API_VERSION = '3.0.0'
    API_URI_VERSION = 'v3'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    DEFAULT_RENDERERS = ['flask_api.renderers.JSONRenderer']


class DevelopmentConfig(Config):
    LOGLEVEL = logging.DEBUG
    DEBUG = True
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    DEFAULT_RENDERERS = [
        'flask_api.renderers.JSONRenderer',
        'flask_api.renderers.BrowsableAPIRenderer',
    ]


class ProductionConfig(Config):
    LOGLEVEL = logging.WARNING
    DEBUG = False


use_config = ProductionConfig()
